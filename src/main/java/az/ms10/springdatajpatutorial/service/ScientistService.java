package az.ms10.springdatajpatutorial.service;

import az.ms10.springdatajpatutorial.dto.ScientistDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ScientistService {
    List<ScientistDto> getScientists();

    ScientistDto getScientistsByID(int id);

    List<ScientistDto> getScientistsByName(String name);

    List<ScientistDto> getScientistsBySurname(String name);

    List<ScientistDto> getScientistByParams(String name, String surname, int age);

    ScientistDto saveScientist(ScientistDto dto);

    ScientistDto updateScientist(int id, ScientistDto dto);

    void deleteScientist(int id);
}
