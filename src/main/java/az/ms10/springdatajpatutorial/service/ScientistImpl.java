package az.ms10.springdatajpatutorial.service;

import az.ms10.springdatajpatutorial.domain.Scientist;
import az.ms10.springdatajpatutorial.dto.ScientistDto;
//import az.ms10.springdatajpatutorial.mapper.ScientistMapper;
import az.ms10.springdatajpatutorial.dto.ScientistDtoMapper;
import az.ms10.springdatajpatutorial.exception.NonexistentIdException;
import az.ms10.springdatajpatutorial.mapper.ScientistMapper;
import az.ms10.springdatajpatutorial.repository.ScientistRepo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScientistImpl implements ScientistService {
    private final ScientistRepo scientistRepo;

    @Override
    public List<ScientistDto> getScientists() {
        return ScientistMapper.INSTANCE.toDto(scientistRepo.findAll());
    }

    @Override
    public ScientistDto getScientistsByID(int id) {
        Optional<Scientist> optionalScientist = scientistRepo.findById(id);

        if (optionalScientist.isPresent()) return ScientistMapper.INSTANCE.toDto(optionalScientist.get());
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND,String.format("Scientist with id %s not found", id));
    }

    @Override
    public List<ScientistDto> getScientistsByName(String name) {
        System.out.println(name);
        List<Scientist> byName = scientistRepo.findByName(name);
        List<ScientistDto> dtoList = ScientistMapper.INSTANCE.toDto(byName);
        return dtoList;
    }

    @Override
    public List<ScientistDto> getScientistsBySurname(String surname) {
        System.out.println(surname);
        List<Scientist> byName = scientistRepo.findBySurname(surname);
        List<ScientistDto> dtoList = ScientistMapper.INSTANCE.toDto(byName);
        return dtoList;
    }

    @Override
    public List<ScientistDto> getScientistByParams(String name, String surname, int age) {
        return ScientistMapper.INSTANCE.toDto(scientistRepo.findByNameOrSurnameOrAge(name, surname, age));
    }

    @Override
    public ScientistDto saveScientist(ScientistDto dto) {
        return ScientistMapper.INSTANCE.toDto(scientistRepo.save(ScientistMapper.INSTANCE.toEntity(dto)));
    }

    @Override
    public ScientistDto updateScientist(int id, ScientistDto dto) {
        dto.setId(id);

        if (scientistRepo.existsById(id)) {
            return ScientistMapper.INSTANCE.toDto(scientistRepo.save(ScientistMapper.INSTANCE.toEntity(dto)));
        } else {
            throw new NonexistentIdException();
        }
    }

    @Override
    public void deleteScientist(int id) {
        if (scientistRepo.existsById(id)) {
            scientistRepo.deleteById(id);
        } else {
            throw new NonexistentIdException();
        }
    }
}
