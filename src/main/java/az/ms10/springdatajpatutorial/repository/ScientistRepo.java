package az.ms10.springdatajpatutorial.repository;

import az.ms10.springdatajpatutorial.domain.Scientist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScientistRepo extends JpaRepository<Scientist, Integer> {

    // Method Query
    List<Scientist> findByNameOrSurnameOrAge(String name, String surname, int age);

    // JPQL
    @Query(nativeQuery = true, value = "select * from scientist s where s.name like %?1%")
    List<Scientist> findByName(String name);

    // Custom query
    @Query("select s from Scientist s where s.surname like %:surname%")
    List<Scientist> findBySurname(@Param("surname") String s);

}
